import { Col, Container, Row } from "react-bootstrap"

import DaoraSection from "../components/Home/SectionDaora"
import EnotripSection from "../components/Home/SectionEno"
import GulaSection from "../components/Home/SectionGula"
import Header from "../components/general/Header"
import HeaderBurger from "../components/general/HeaderBurger"
import HeroSection from "../components/Home/HeroSection"
import HomeFooter from "../components/general/Footer"
import PartnersSection from "../components/Home/SectionPartners"
import React from "react"
import TaçaSection from "../components/Home/SectionGlass"

const IndexPage = () => (
  <Container>
    <HeaderBurger />
    <Header />
    <HeroSection />
    <DaoraSection />
    <TaçaSection />
    <GulaSection />
    <div style={{ position: "relative" }}>
      <EnotripSection />
      <PartnersSection />
    </div>
    <HomeFooter />
  </Container>
)

export default IndexPage
