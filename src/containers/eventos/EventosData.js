import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Eventos from "./Eventos"

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressPost(
          filter: { categories: { elemMatch: { slug: { eq: "eventos" } } } }
        ) {
          nodes {
            content
            date(formatString: "DD/MMMM/YYYY", locale: "pt-br")
            title
            featured_media {
              source_url
            }
            wordpress_id
          }
        }
      }
    `}
    render={data => <Eventos data={data.allWordpressPost.nodes} />}
  ></StaticQuery>
)

export default ComponentName
