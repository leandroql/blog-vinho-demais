import { Container } from "react-bootstrap"
import Header from "../../components/general/HeaderPost"
import HeaderBurger from "../../components/general/HeaderBurger"
import PostContent from "../../components/Post/Core"
import React, { useEffect } from "react"
import Title from "../../components/Post/Title/Title"
import eventos from "../../assets/images/post/eventos.png"
import icon from "../../assets/images/post/SETA_VOLTAR.png"

const getPosts = data => {
  return data.map(item => {
    return <PostContent data={item} category={"Eventos"} />
  })
}
const Enotrip = ({ data }) => {
  useEffect(() => {
    if (window.location.href.search("#") >= 0) {
      window.location.href = `#${window.location.href.split("#").pop()}`
      const b = window.location.href.split("#").pop()
      document.getElementById(b).scrollIntoView({ behavior: "smooth" })
    }
  }, [])
  return (
    <>
      <Header />
      <HeaderBurger />
      <div
        style={{
          position: "absolute",
          zIndex: 9999,
        }}
      >
        <div className="midia-item-container-post">
          <div>
            <a href={"/"}>
              <img width={45} heigth={45} src={icon} />
            </a>
          </div>
        </div>
      </div>
      <Title image={eventos} />
      <Container>{getPosts(data)}</Container>
    </>
  )
}

export default Enotrip
