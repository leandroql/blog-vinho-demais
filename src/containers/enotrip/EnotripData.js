import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Enotrip from "./Enotrip"

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressPost(
          filter: { categories: { elemMatch: { slug: { eq: "enotrip" } } } }
        ) {
          nodes {
            content
            date(formatString: "DD/MMMM/YYYY", locale: "pt-br")
            title
            featured_media {
              source_url
            }
            wordpress_id
          }
        }
      }
    `}
    render={data => <Enotrip data={data.allWordpressPost.nodes} />}
  ></StaticQuery>
)

export default ComponentName
