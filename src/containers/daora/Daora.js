import React, { useEffect } from "react"
import PostContent from "../../components/Post/Core"
import Header from "../../components/general/HeaderPost"
import HeaderBurger from "../../components/general/HeaderBurger"
import Title from "../../components/Post/Title/Title"
import daora from "../../assets/images/post/vinho-daora.png"
import icon from "../../assets/images/post/SETA_VOLTAR.png"

const getPosts = data => {
  return data.map(item => {
    return (
      <div id={item.wordpress_id}>
        <PostContent data={item} category={"Vinho Daora"} />
      </div>
    )
  })
}
const Daora = ({ data }) => {
  useEffect(() => {
    if (window.location.href.search("#") >= 0) {
      window.location.href = `#${window.location.href.split("#").pop()}`
      const b = window.location.href.split("#").pop()
      document.getElementById(b).scrollIntoView({ behavior: "smooth" })
    }
  }, [])
  return (
    <>
      <Header />
      <HeaderBurger />
      <div
        style={{
          position: "absolute",
          zIndex: 9999,
        }}
      >
        <div className="midia-item-container-post">
          <div>
            <a href={"/"}>
              <img width={45} heigth={45} src={icon} />
            </a>
          </div>
        </div>
      </div>
      <Title image={daora} />
      {getPosts(data)}
    </>
  )
}

export default Daora
