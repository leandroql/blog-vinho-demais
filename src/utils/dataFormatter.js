export const formatPostContent = (content) => {
    const b = content.replace(/(<([^>]+)>)/ig, "").trim()
    return b.replace(/&(.*);/ig, function (x) {
        const teste = parseInt(x.slice(2, -1), 10)
        return String.fromCharCode(teste)
    })
}

export const formatPostDate = (date) => {
    let aux = date.replace("/", " de ")
    const newDate = aux.replace("/", " de ")
    return newDate
}

export const formatPostThumbDate = (date) => {
    const newDate = date.replace("-", " , ")
    return newDate
}

export const formatImage = (content) => {
}

const decode_utf8 = (s) => {
    return decodeURIComponent(escape(s));
}

const a = (string) => {
    string.replace(/&(.*);/ig, function (x) {
        const teste = parseInt(x.slice(2, -1), 10)
        return String.fromCharCode(teste)
    })
}

const limpa = (str) => {
    str = str.replace("/<(p|/p|br|/br)>/", "")
    //str = decode_utf8(str)
    return str
}

export const limpaComRegex = (aTratar) => {
    const dados = Object.keys(aTratar)

    dados.forEach(dado => {
        if (aTratar[dado]) {
            aTratar[dado] = limpa(aTratar[dado])
        }
    })
    return aTratar
}
