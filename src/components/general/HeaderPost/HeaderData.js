import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Header from './Header'

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressWpApiMenusMenusItems {
          nodes {
            items {
              title
              url
            }
          }
        }
      }
    `}
    render={data => <Header data={data.allWordpressWpApiMenusMenusItems.nodes[0].items} />}
  ></StaticQuery >
)

export default ComponentName
