import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Burger from './Burger'

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressWpApiMenusMenusItems {
          nodes {
            items {
              title
              url
            }
          }
        }
      }
    `}
    render={data => <Burger data={data.allWordpressWpApiMenusMenusItems.nodes[0].items} />}
  ></StaticQuery >
)

export default ComponentName
