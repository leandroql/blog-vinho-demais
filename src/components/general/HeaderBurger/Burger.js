import React from "react"
import { slide as Menu } from "react-burger-menu"
import { logo_taca } from "../../../utils/icons"

const renderItems = data => {
  return data.map(item => {
    if (item.title !== "Home") {
      return (
        <a className="menu-item" href={item.url}>
          {item.title}
        </a>
      )
    }
  })
}
const Burger = ({ data }) => {
  return (
    <section className="section-burger">
      <div className="img">{logo_taca}</div>
      <Menu>
        <a className="menu-item" href={"/"}>
          Home
        </a>
        {renderItems(data)}
      </Menu>
    </section>
  )
}
export default Burger
