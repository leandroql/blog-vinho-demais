import React, { useEffect, useState } from "react"
import { logo_letras, logo_taca } from "../../../utils/icons"

import { Navbar } from "react-bootstrap"

const renderMenuItems = items => {
  return items.map(item => {
    if (item.title !== "Home") {
      return (
        <div className="item">
          <a href={item.url}>{item.title}</a>
        </div>
      )
    }
  })

}
const Header = ({ data }) => {
  const [targetClass, setTargetClass] = useState("img-container")

  useEffect(() => {
    window.addEventListener("scroll", onScroll)
  }, [])

  const onScroll = e => {
    if (window.pageYOffset <= 10) {
      setTargetClass("img-container")
    } else {
      setTargetClass("image-container-scroll")
    }
  }

  return (
    <header id="main-header">
      <div className="nav-flex-wrap">
        <div className={targetClass}>
          <div className="img">
            {logo_taca}
            {targetClass !== "image-container-scroll" ? logo_letras : null}
          </div>
        </div>
        <div className="item-wrap">{renderMenuItems(data)}</div>
      </div>
    </header>
  )
}

export default Header
