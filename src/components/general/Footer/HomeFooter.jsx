import { logo_letras, logo_taca } from "../../../utils/icons"

import React from "react"

const HomeFooter = ({}) => {
  return (
    <footer className={"footer"}>
      <div className="img">
        {logo_taca}
        {logo_letras}
      </div>
    </footer>
  )
}

export default HomeFooter
