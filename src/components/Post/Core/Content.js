import { Col, Row } from "react-bootstrap"
import { formatPostContent, formatPostDate } from "../../../utils/dataFormatter"

import React from "react"
import teste from "../../../assets/images/post/teste.jpg"

const PostContent = ({ data, category }) => {
  return (
    <div style={{ backgroundColor: "#5e182d" }} className="post-content">
      <div className="container">
        <div className="post-content-container">

          {data.featured_media ? <img width={426} height={570} src={data.featured_media.source_url} /> : null}

          <div className="post-card">
            <div className="post-card-category">
              <h2>{category}</h2>
            </div>
            <div className="post-card-title">
              <h2>{data.title}</h2>
              <div dangerouslySetInnerHTML={{ __html: data.content }}></div>
            </div>

            <hr />
            <div className="post-card-author">
              {/* <img /> */}
              <h2>Nara Cavaquioli</h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default PostContent
