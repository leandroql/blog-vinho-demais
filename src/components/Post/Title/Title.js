import React from "react"
import background from "../../../assets/images/post/capa.svg"

const Title = ({ image }) => {
  return (
    <div className="background-container-post">
      <div
        style={{
          backgroundImage: `url(${background})`,
          display: "flex",
          justifyContent: "center",
          backgroundSize: "cover",
        }}
      >
        <img width={"50%"} src={image} />
      </div>
    </div>
  )
}

export default Title
