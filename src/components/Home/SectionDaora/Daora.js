import {
  formatPostContent,
  formatPostThumbDate,
  limpaComRegex,
} from "../../../utils/dataFormatter"

import React from "react"
import cardTest from "../../../assets/images/home/card.svg"
import elipse from "../../../assets/images/home/Elipse1.svg"
import uva from "../../../assets/images/home/BLOG_VINHODEMAIS_FUNDOUVA.png"

const Daora = ({ data, postData }) => {
  const length = postData.length
  const content1 = formatPostContent(postData[0].content)
  const content2 = formatPostContent(postData[1].content)
  return (
    <div
      className="section-daora"
      style={{
        backgroundImage: `url(${uva})`,
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        minHeight: "1200px",
      }}
    >
      <div className="container">
        <div className="relative">
          <div className="relative-column">
            <div className="relative-wrapper-title">
              <div className="relative-category-title">
                <img src={elipse} />
                <h2>ENOTRIP</h2>
              </div>
              <h2>{data.enotrip_title}</h2>
            </div>
            <div className="relative-card-container">
              <div className="flex" style={{ justifyContent: "center" }}>
                <div className="relative-card-absolute">
                  <a href={`/enotrip/#${postData[0].wordpress_id}`}>
                    <img
                      className="relative-blog-image"
                      width={276}
                      height={370}
                      src={postData[0].featured_media.source_url}
                    />
                    <img
                      width={276}
                      height={370}
                      className="relative-purple-card"
                      src={cardTest}
                    />
                    <div className="relative">
                      <div className="post-card-category">
                        <h2>Enotrip</h2>
                      </div>
                      <div className="post-card-title">
                        <h2>{postData[0].title}</h2>
                        <div className="post-content">{content1}</div>
                      </div>
                      <div className="post-card-author">
                        <span>Nara Cavaquioli</span>
                      </div>
                    </div>
                  </a>
                </div>
                <div className="relative-card-absolute">
                  <a href={`/enotrip/#${postData[1].wordpress_id}`}>
                    <img
                      className="relative-blog-image"
                      width={276}
                      height={370}
                      src={postData[1].featured_media.source_url}
                    />
                    <img
                      width={276}
                      height={370}
                      className="relative-purple-card"
                      src={cardTest}
                    />
                    <div className="relative">
                      <div className="post-card-category">
                        <h2>Enotrip</h2>
                      </div>
                      <div className="post-card-title">
                        <h2>{postData[1].title}</h2>
                        <div className="post-content">{content2}</div>
                      </div>
                      <div className="post-card-author">
                        <span>Nara Cavaquioli</span>
                      </div>
                    </div>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Daora
