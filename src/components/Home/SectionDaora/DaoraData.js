import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Daora from "./Daora"

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressPost(
          filter: { categories: { elemMatch: { slug: { eq: "enotrip" } } } }
        ) {
          nodes {
            title
            content
            date(formatString: "MMMM,DD-YYYY", locale: "pt-br")
            featured_media {
              source_url
            }
            wordpress_id
          }
        }
        allWordpressPage(filter: { slug: { eq: "home" } }) {
          nodes {
            acf {
              enotrip_title
            }
          }
        }
      }
    `}
    render={data => (
      <Daora
        data={data.allWordpressPage.nodes[0].acf}
        postData={data.allWordpressPost.nodes}
      />
    )}
  ></StaticQuery>
)

export default ComponentName
