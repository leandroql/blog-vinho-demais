import React from "react"
import elipse from "../../../assets/images/home/Elipse1.svg"
import adega from "../../../assets/images/home/adega.png"

const getUrl = (partner, icons) => {
  let a = ""
  icons.map(item => {
    if (item.title === partner) {
      a = item.source_url
    }
  })
  return a
}

const renderItems = ({ partners }, icons) => {
  return partners.map(item => {
    return (
      <div className="partners-wrapper-container">
        <div className="partners-wrapper-logo">
          <img width={150} src={getUrl(item.partner_social_media, icons)} />
        </div>
        <h2>{item.partner_name}</h2>
        <p>{item.partner_social_media}</p>
      </div>
    )
  })
}

const Partners = ({ data, icons }) => {
  return (
    <section
      id="marcas-parceiros"
      className="section section-partners"
      style={{ backgroundImage: `url(${adega})`, backgroundSize: "cover" }}
    >
      <div className="container">
        <div style={{ position: "relative" }}>
          <div className="flex-wrapper">
            <div className="flex-wrapper-category">
              <img src={elipse} />
              <h2
                style={{
                  fontWeight: 900,
                  color: "white",
                  fontSize: "17px",
                  paddingLeft: "15px",
                }}
              >
                MARCAS E PARCEIROS
              </h2>
            </div>
            <div className="partners-wrapper">{renderItems(data, icons)}</div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Partners
