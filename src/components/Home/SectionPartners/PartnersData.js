import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Partners from './Partners'

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressPage(filter: {slug: {eq: "home"}}) {
          nodes {
            acf {
              partners {
                partner_name
                partner_social_media
              }
            }
          }
        }
        allWordpressWpMedia {
          nodes {
            source_url
            title
          }
        }
      }
    `}
    render={data => <Partners data={data.allWordpressPage.nodes[0].acf} icons={data.allWordpressWpMedia.nodes} />}
  ></StaticQuery>
)

export default ComponentName
