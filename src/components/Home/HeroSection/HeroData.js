import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Hero from './Hero'

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressPage(filter: {slug: {eq: "home"}}) {
          nodes {
            acf {
              hero_text
              hero_title
              side_card
              social_media {
                social_media_href
                social_media_icon
              }
            }
          }
        }
        allWordpressWpMedia(filter: {alt_text: {eq: "Midia"}}) {
          nodes {
            source_url
            title
          }
        }
      }
    `}
    render={data => <Hero data={data.allWordpressPage.nodes[0].acf} icons={data.allWordpressWpMedia.nodes} />}
  ></StaticQuery>
)

export default ComponentName
