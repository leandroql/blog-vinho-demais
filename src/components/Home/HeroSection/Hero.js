import React from "react"
import cardTeste from "../../../assets/images/home/card.svg"
import background from "../../../assets/images/home/nara.png"

const renderMidiaItems = (data, icons) => {
  return icons.map(icon => {
    return data.social_media.map(item => {
      if (item.social_media_icon === icon.title) {
        return (
          <div className="midia-item-container-home">
            <a href={item.social_media_href}>
              <img width={45} heigth={45} src={icon.source_url} />
            </a>
          </div>
        )
      }
    })
  })
}

const HeroSection = ({ data, icons }) => {
  return (
    <section className="hero-section">
      <div className="photo-container" >
        <div className="image-container">
          <div className="midia-container">{renderMidiaItems(data, icons)}</div>
        </div>
      </div>
      <div class="heroRow">
        <div
          className="textHero"
          style={{
            backgroundImage: `url(${cardTeste})`,
            width: "610px",
            height: "310px",
          }}
        >
          <h2
            style={{
              position: "unset",
              fontWeight: 900,
              fontSize: "29px",
              padding: "5px 20px",
              textTransform: "uppercase",
            }}
          >
            {data.hero_title}
          </h2>
          <p
            style={{
              position: "unset",
              lineHeight: "1.5em",
              minWidth: "100%",
              fontWeight: "300",
              fontSize: "15px",
              padding: "20px",
            }}
          >
            {data.hero_text}
          </p>
        </div>
        <div
          className="mediaKit"
          style={{ maxWidth: "46px", maxHeight: "230px", cursor: "pointer" }}
        >
          <a href={"https://kuak.com/vinhosdemais"}>MIDIA KIT</a>
        </div>
      </div>
    </section>
  )
}

export default HeroSection
