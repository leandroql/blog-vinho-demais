import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Gula from "./Gula"

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressPost(
          filter: { categories: { elemMatch: { slug: { eq: "da-gula" } } } }
        ) {
          nodes {
            title
            content
            date(formatString: "MMMM,DD-YYYY", locale: "pt-br")
            featured_media {
              source_url
            }
            wordpress_id
          }
        }
        allWordpressPage(filter: { slug: { eq: "home" } }) {
          nodes {
            acf {
              gula_title
            }
          }
        }
      }
    `}
    render={data => (
      <Gula
        data={data.allWordpressPage.nodes[0].acf}
        postData={data.allWordpressPost.nodes}
      />
    )}
  ></StaticQuery>
)

export default ComponentName
