import React from "react"
import elipse from "../../../assets/images/home/Elipse1.svg"
import teste from "../../../assets/images/home/22.jpg"

const Gula = ({ data, postData }) => {
  const length = postData.length
  return (
    <section className="section-gula">
      <div className="container">
        <div className="flex-wrapper">
          <div className="flex-wrapper-title">
            <div className="flex-wrapper-category">
              <img src={elipse} />
              <h2 className="category">DA GULA</h2>
            </div>
            <h2 className="wrapper-column-title">{data.gula_title}</h2>
          </div>
          <div className="wrapper-column">
            <div className="container">
              <div className="flex" style={{ justifyContent: "center" }}>
                <div className="flex">
                  <div className="card-container">
                    <a href={`/da-gula/#${postData[0].wordpress_id}`}>
                      <img
                        width={220}
                        height={295}
                        src={postData[0].featured_media.source_url}
                      />
                      <div className="card-container-wrapper">
                        <div className="card-container-title-wrapper">
                          {/* <h2 style={{ fontSize: '15px', alignSelf: "start", padding: '25px' }}>Place</h2> */}
                          <h2
                            style={{
                              maxWidth: "300px",
                              padding: "35px",
                              fontSize: "12px",
                            }}
                          >
                            {postData[0].title}
                          </h2>
                        </div>
                        <div className="card-container-author-wrapper">
                          <img width={15} src={elipse} />
                          <p>Nara Caviquioli</p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div className="flex">
                  <div className="card-container">
                    {/* <div className="card-container-image"> */}
                    <a href={`/da-gula/#${postData[1].wordpress_id}`}>
                      <img
                        width={220}
                        height={295}
                        src={postData[1].featured_media.source_url}
                      />
                      {/* </div> */}
                      <div className="card-container-wrapper">
                        <div className="card-container-title-wrapper">
                          {/* <h2 style={{ fontSize: '15px', alignSelf: "start", padding: '25px' }}>Place</h2> */}
                          <h2
                            style={{
                              maxWidth: "300px",
                              padding: "35px",
                              fontSize: "12px",
                            }}
                          >
                            {postData[1].title}
                          </h2>
                        </div>
                        <div className="card-container-author-wrapper">
                          <img width={15} src={elipse} />
                          <p>Nara Caviquioli</p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div className="flex">
                  <div className="card-container">
                    <a href={`/da-gula/#${postData[2].wordpress_id}`}>
                      <img
                        width={220}
                        height={295}
                        src={postData[2].featured_media.source_url}
                      />
                      <div className="card-container-wrapper">
                        <div className="card-container-title-wrapper">
                          {/* <h2 style={{ fontSize: '15px', alignSelf: "start", padding: '25px' }}>Place</h2> */}
                          <h2
                            style={{
                              maxWidth: "300px",
                              padding: "35px",
                              fontSize: "12px",
                            }}
                          >
                            {postData[2].title}
                          </h2>
                        </div>
                        <div className="card-container-author-wrapper">
                          <img width={15} src={elipse} />
                          <p>Nara Caviquioli</p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
export default Gula
