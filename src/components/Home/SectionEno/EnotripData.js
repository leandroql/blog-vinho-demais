import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Eno from "./Eno"

const ComponentName = () => (
  <StaticQuery
    query={graphql`
      {
        allWordpressPost(
          filter: { categories: { elemMatch: { slug: { eq: "vinho-daora" } } } }
        ) {
          nodes {
            title
            content
            date(formatString: "MMMM,DD-YYYY", locale: "pt-br")
            featured_media {
              source_url
            }
            wordpress_id
          }
        }
        allWordpressPage(filter: { slug: { eq: "home" } }) {
          nodes {
            acf {
              daora_title
            }
          }
        }
      }
    `}
    render={data => (
      <Eno
        data={data.allWordpressPage.nodes[0].acf}
        postData={data.allWordpressPost.nodes}
      />
    )}
  ></StaticQuery>
)

export default ComponentName
