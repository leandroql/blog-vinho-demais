import React from "react"
import elipse from "../../../assets/images/home/Elipse1.svg"
import a from "../../../assets/images/home/chalk.svg"

const Eno = ({ data, postData }) => {
  return (
    <section
      className="section-eno"
      style={{
        backgroundImage: `url(${a})`,
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
      }}
    >
      <div className="container">
        <div className="flex-wrapper">
          <div className="flex-wrapper-title">
            <div className="flex-wrapper-category">
              <img src={elipse} />
              <h2 className="category">VINHO DAORA</h2>
            </div>
            <h2 className="wrapper-column-title" style={{ color: "white" }}>
              {data.daora_title}
            </h2>
          </div>
          <div className="wrapper-column">
            <div className="container">
              <div className="flex" style={{ justifyContent: "center" }}>
                <div className="flex">
                  <div className="card-container">
                    <a
                      href={`/vinho-daora/#${
                        postData[0].wordpress_id
                        }`}
                    >
                      <img
                        width={220}
                        height={295}
                        src={postData[0].featured_media.source_url}
                      />
                      <div className="card-container-wrapper">
                        <div className="card-container-title-wrapper">
                          <h2
                            style={{
                              maxWidth: "300px",
                              padding: "35px",
                              fontSize: "12px",
                            }}
                          >
                            {postData[0].title}
                          </h2>
                        </div>
                        <div className="card-container-author-wrapper">
                          <img width={15} src={elipse} />
                          <p>Nara Caviquioli</p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div className="flex">
                  <div className="card-container">
                    <a
                      href={`/vinho-daora/#${
                        postData[1].wordpress_id
                        }`}
                    >
                      <img
                        width={220}
                        height={295}
                        src={postData[1].featured_media.source_url}
                      />
                      <div className="card-container-wrapper">
                        <div className="card-container-title-wrapper">
                          <h2
                            style={{
                              maxWidth: "300px",
                              padding: "35px",
                              fontSize: "12px",
                            }}
                          >
                            {postData[1].title}
                          </h2>
                        </div>
                        <div className="card-container-author-wrapper">
                          <img width={15} src={elipse} />
                          <p>Nara Caviquioli</p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
                <div className="flex">
                  <div className="card-container">
                    <a
                      href={`/vinho-daora/#${
                        postData[2].wordpress_id
                        }`}
                    >
                      <img
                        width={220}
                        height={295}
                        src={postData[2].featured_media.source_url}
                      />
                      <div className="card-container-wrapper">
                        <div className="card-container-title-wrapper">
                          {/* <h2 style={{ fontSize: '15px', alignSelf: "start", padding: '25px' }}>Place</h2> */}
                          <h2
                            style={{
                              maxWidth: "300px",
                              padding: "35px",
                              fontSize: "12px",
                            }}
                          >
                            {postData[2].title}
                          </h2>
                        </div>
                        <div className="card-container-author-wrapper">
                          <img width={15} src={elipse} />
                          <p>Nara Caviquioli</p>
                        </div>
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}
export default Eno
