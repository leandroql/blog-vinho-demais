import React from "react"
import { StaticQuery, graphql } from "gatsby"
import Glass from './Glass'

const ComponentName = () => (
    <StaticQuery
        query={graphql`
      {
        allWordpressPage(filter: {slug: {eq: "home"}}) {
          nodes {
            acf {
              glass_text
            }
          }
        }
      }
    `}
        render={data => <Glass data={data.allWordpressPage.nodes[0].acf} />}
    ></StaticQuery>
)

export default ComponentName
