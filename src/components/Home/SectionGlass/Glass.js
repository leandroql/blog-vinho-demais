import React from "react"
import teste from "../../../assets/images/home/taça.png"
import cardTeste from "../../../assets/images/home/card.svg"
import aspas from "../../../assets/images/home/aspas.svg"
import nara from "../../../assets/images/home/FOTO_NARA_RECORTE.png"

const Glass = ({ data }) => {
  return (
    <section className="section section-glass">
      <div className="container">
        <div className="glass-container">
          <img className="background" src={teste} />
          <div>
            <div style={{ overflow: "hidden" }}>
              <img
                width={150}
                height={150}
                className="relative-aspas"
                src={aspas}
              />
              <img
                className="relative-card"
                width={500}
                height={550}
                src={cardTeste}
              />
              <h2 className="relative-text">{data.glass_text}</h2>
              <div className="relative-author">
                <img
                  style={{ borderRadius: "50%" }}
                  width={64}
                  height={64}
                  src={nara}
                />
                <h2>Nara Caviquioli</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default Glass
